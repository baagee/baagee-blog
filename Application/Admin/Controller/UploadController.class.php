<?php

namespace Admin\Controller;

/**
 * 文件上传类
 */
class UploadController extends BaseController{

    /**
     * 上传图片  
     * @return [json] [文件路径]
     */
	public function uploadImage(){
		$resdata = array();
        $cfg=array(
            'rootPath'=>'./Upload/',
            'maxSize'=>2145728,
            'exts'=>array('jpg', 'gif', 'png', 'jpeg', 'JPG'),
        );

        if(!empty(I('post.type')) && I('post.type')=='cover'){
            //封面图
            $path='./Upload/cover_images/';//220X144
            $cfg['savePath']='cover_images/';
        }elseif(I('post.type')=='avatar'){
            // 头像
            $path='./Upload/avatar_images/';//150x150
            $cfg['savePath']='avatar_images/';
        }else{
            //文章内容图
    		$path='./Upload/article_images/';
            $cfg['savePath']='article_images/';
        }
		if (!is_dir($path)){
			//目录不存在
			if(!mkdir(iconv("UTF-8", "GBK", $path),0777,true)){
				//没有创建
				$resdata['status'] = 0;
				$resdata['msg']='创建文件夹失败';
				$this->ajaxReturn($resdata);
			}
		}
        $upload = new \Think\Upload($cfg);
        $info = $upload->uploadOne($_FILES['Filedata']);
        if (! $info) {
            $resdata['status'] = 0;
            $resdata['msg'] = $upload->getError();
        } else {
            $imgpath=$cfg['rootPath'].$info['savepath'] . $info['savename'];
            $resdata['status'] = 1;
            $image = new \Think\Image();
            $image->open($imgpath);
            if(!empty(I('post.type')) && I('post.type')=='cover'){
                if(end(explode('.', $imgpath))=='gif'){
                    unlink($imgpath);
                    $this->ajaxReturn(array('status'=>0,'msg'=>'不能上传动态图'));
                }
                /*文章封面小图*/
                $image->thumb(220, 144,\Think\Image::IMAGE_THUMB_CENTER);
                if(isset(F('webSet')['picwater']) && isset(F('webSet')['picwaterfontsize']) && isset(F('webSet')['picwaterfontcolor'])){
                    $image->text(F('webSet')['picwater'],'./Public/static/admin/i/fh.ttf',F('webSet')['picwaterfontsize'],F('webSet')['picwaterfontcolor'],\Think\Image::IMAGE_WATER_SOUTHEAST);    
                }
                $image->save($cfg['rootPath'].$info['savepath'] . 'thumb_220X144_'.$info['savename']);
                // 生成一个居中裁剪为730*272的缩略图并保存 文章封面大图
                $image->open($imgpath)->thumb(730, 272,\Think\Image::IMAGE_THUMB_CENTER);//用原图操作
                if(isset(F('webSet')['picwater']) && isset(F('webSet')['picwaterfontsize']) && isset(F('webSet')['picwaterfontcolor'])){
                    $image->text(F('webSet')['picwater'],'./Public/static/admin/i/fh.ttf',F('webSet')['picwaterfontsize'],F('webSet')['picwaterfontcolor'],\Think\Image::IMAGE_WATER_SOUTHEAST);    
                }
                // ->text('BaAGee','./Public/static/admin/i/fh.ttf',15,'#1db3e4',\Think\Image::IMAGE_WATER_SOUTHEAST)
                $image->save($cfg['rootPath'].$info['savepath'] . 'thumb_730X272_'.$info['savename']);

                $resdata['msg'] = trim($cfg['rootPath'].$info['savepath'] . 'thumb_220X144_'.$info['savename'],'.');
                unlink($imgpath);//删除原图
            }elseif(!empty(I('post.type')) && I('post.type')=='avatar'){
                if(end(explode('.', $imgpath))=='gif'){
                    unlink($imgpath);
                    $this->ajaxReturn(array('status'=>0,'msg'=>'不能上传动态图'));
                }
                /*头像*/
                $image->thumb(150, 150,\Think\Image::IMAGE_THUMB_CENTER)
                ->save($cfg['rootPath'].$info['savepath'] . 'avatar_150X150_'.$info['savename']);
                $resdata['msg'] = trim($cfg['rootPath'].$info['savepath'] . 'avatar_150X150_'.$info['savename'],'.');
                unlink($imgpath);//删除原图
            }else{
                /*文章内容插图*/
                if(end(explode('.', $imgpath))!='gif'){
                    if(isset(F('webSet')['picwater']) && isset(F('webSet')['picwaterfontsize']) && isset(F('webSet')['picwaterfontcolor'])){
                        $image->text(F('webSet')['picwater'],'./Public/static/admin/i/fh.ttf',F('webSet')['picwaterfontsize'],F('webSet')['picwaterfontcolor'],\Think\Image::IMAGE_WATER_SOUTHEAST);
                    } 
                    $image->save($imgpath);
                }
                $resdata['msg'] = trim($imgpath,'.');
            }
        }
        $this->ajaxReturn($resdata);
	}
}