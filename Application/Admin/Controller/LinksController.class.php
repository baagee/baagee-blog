<?php

namespace Admin\Controller;
use Think\Controller;

class LinksController extends BaseController{

	/**
	 * 管理友情链接
	 */
	public function index(){
		$this->links=M('links')->select();
		$this->title='友情链接管理';
		$this->display();
	}

	/**
	 * 删除友情链接
	 */
	public function deleteLink(){
		if(IS_AJAX){
			if(M('links')->delete(I('post.id'))){
                $this->ajaxReturn(array('status'=>1,'msg'=>'删除完成'));
            }else{
                $this->ajaxReturn(array('status'=>0,'msg'=>'删除失败'));
            } 
		}else{
			$this->error('访问错误');
		}
	}

	/**
	 * 审核友情链接
	 */
	public function status(){
		if(IS_AJAX && !empty(I('post.id'))){
			$status=M('links')->field('status')->find(I('post.id'))['status']?0:1;
			if(M('links')->save(array('id'=>I('post.id'),'status'=>$status))){
				if(F('webSet')['Semail']==1){
					if($status==1){
						$toEmail=M('links')->field('email')->find(I('post.id'))['email'];
						if(!empty($toEmail)){
							$content='你好！你在'.F('webSet')['webtitle'].'网站提交的友情链接审核通过啦！Thanks';
		                    $res=sendMail($toEmail,'友情链接审核通知',$content);
		                    if($res!==true){
		                    	$this->ajaxReturn(array('status'=>0,'msg'=>'修改完成,但是邮件发送失败:'.$res,'data'=>$status));
		                    }
						}
					}
				}
	            $this->ajaxReturn(array('status'=>1,'msg'=>'修改完成','data'=>$status));
	        }else{
	            $this->ajaxReturn(array('status'=>0,'msg'=>'修改失败'));
	        }
		}else{
			$this->error('访问错误');	
		}
	}
	
	/**
	 * 更改友情链接字段
	 */
	public function updateField(){
        if(IS_AJAX && I('post.update')==1){
            if(M('links')->save(array('id'=>I('post.id'),I('post.field')=>I('post.value')))){
                 $this->ajaxReturn(array('status'=>1,'msg'=>'修改完成'));
            }else{
                $this->ajaxReturn(array('status'=>0,'msg'=>'修改失败'));
            }
        }else{
        	$this->error('访问错误');
        }
	}

}
