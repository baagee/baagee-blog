<?php
namespace Admin\Controller;
use Think\Verify;

class UserController extends BaseController{
    /**
     * 验证码
     */
	public function verify(){
        $cfg=array(
            'imageH'=>37,       //验证码高度
            'imageW'=>120,      //验证码宽度
            'fontSize'=>16,     //验证码字体大小
            'length'=>4,        //验证码字符串长度
            'fontttf'=>'5.ttf'  //验证码字体
        );
        $very=new Verify($cfg);
        $very->entry();
    }

    /**
     * 后台登录
     */
    public function login(){
        if(IS_AJAX){
            $username=I('post.username');
            $password=I('post.password');
            if(trim($username)=='' || trim($password)==''){
                $this->ajaxReturn(array('status'=>0,'msg'=>'用户名或密码不能为空'));
            }
            $verify=new Verify();
            if(!$verify->check(I('post.verify'))){
                $this->ajaxReturn(array('status'=>0,'msg'=>'验证码错误！'));
            }
            if($userinfo=M('user')->where(array('username'=>$username,'password'=>makePassword($password)))->select()){
                session('userinfo',$userinfo);
                // 更新登录信息
                $data=array(
                    'login_ip'=>$_SERVER["REMOTE_ADDR"],
                    'last_login_time'=>$userinfo[0]['login_time'],
                    'login_time'=>time(),'id'=>$userinfo[0]['id']
                );
                M('user')->save($data);
                if(F('webSet')['Semail']==1){
                    // 给我发邮件通知
                    $myEmail=M('user')->field('email')->limit(1)->select()[0]['email'];
                    if(!empty($myEmail)){
                        $content='你刚才登陆了博客,<br>如果不是你本人登陆，请立即修改密码';
                        sendMail($myEmail,'博客登录通知',$content);
                    }
                }
                $this->ajaxReturn(array('status'=>1,'msg'=>'登陆成功'));
            }else{
                $this->ajaxReturn(array('status'=>0,'msg'=>'用户名或者密码错误'));
            }
        }
        $this->title='后台登录';
        if(isset(F('webSet')['loginType']) && F('webSet')['loginType']=='login2'){
            $tmpName='login2';
        }else{
            $tmpName='login';
        }
        $this->display($tmpName);
    }

    /**
     * 登出
     */
    public function logout(){
        session('userinfo',null);
        $this->redirect('login');
    }

    /**
     * 修改信息
     */
    public function userInfo(){
        $data=I("post.");
        if(IS_AJAX){
            if(isset($data['oldpass']) && isset($data['newpass'])){
                /*修改密码*/
                if(trim($data['newpass'])==''){
                    $this->ajaxReturn(array('status'=>0,'msg'=>'新密码不能为空'));
                }
                if(M('user')->where(array('username'=>session('userinfo')[0]['username'],'password'=>makePassword($data['oldpass'])))->select()){
                    if(M('user')->save(array('id'=>session('userinfo')[0]['id'],'password'=>makePassword($data['newpass'])))){
                        $this->ajaxReturn(array('status'=>1,'msg'=>'密码修改完成'));
                    }else{
                        $this->ajaxReturn(array('status'=>0,'msg'=>'密码修改失败'));
                    }
                }else{
                    $this->ajaxReturn(array('status'=>0,'msg'=>'旧密码错误'));
                }
            }elseif(isset($data['avatar'])){
                /*修改头像*/
                $oldAvatar=M('user')->field('avatar')->find(session('userinfo')[0]['id'])['avatar'];
                if(file_exists('.'.$oldAvatar)){
                    unlink('.'.$oldAvatar);
                }
                if(M('user')->save(array('id'=>session('userinfo')[0]['id'],'avatar'=>$data['avatar'],'update_time'=>time()))){
                    // 更新用户session
                    $userinfo=session('userinfo');
                    $userinfo[0]['avatar']=$data['avatar'];
                    session('userinfo',$userinfo);
                    $this->ajaxReturn(array('status'=>1,'msg'=>'修改完成'));
                }else{
                    $this->ajaxReturn(array('status'=>0,'msg'=>'修改失败'));
                }
            }else{
                /*修改信息*/
                $data['id']=session('userinfo')[0]['id'];
                $data['update_time']=time();
                if(M('user')->save($data)){
                    $this->ajaxReturn(array('status'=>1,'msg'=>'修改完成'));
                }else{
                    $this->ajaxReturn(array('status'=>0,'msg'=>'修改失败'));
                }
            }
        }
        $this->userInfo=$userinfo=M('user')->find(session('userinfo')['id']);
        $this->title='用户资料';
        $this->display();
    }

    /**
     * 重置密码
     */
    public function resetPass(){
        if(I('post.')){
            if(empty(I('post.username'))){
                $this->error('用户名不能为空');
                exit;
            }
            if(empty(I('post.password'))){
                $this->error('密码不能为空');
                exit;
            }
            if($uid=M('user')->field('id')->where('username="'.I('post.username').'"')->find()['id']){
                if(M('user')->save(array('id'=>$uid,'password'=>makePassword(I('post.password'))))){
                    $this->success('修改成功');
                }else{
                    $this->error('修改失败');
                }
                exit;
            }else{
                $this->error('用户名错误');
                exit;
            }
        }
        $this->display();
    }
}