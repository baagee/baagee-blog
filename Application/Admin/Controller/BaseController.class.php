<?php
namespace Admin\Controller;
use Think\Controller;

class BaseController extends Controller{
	public function __construct(){
        parent::__construct();
        /*除了login,verify方法，其他方法都需要登录*/
        if(!(in_array(strtolower(ACTION_NAME),array('login','verify','resetpass')))){
            if(empty(session('userinfo'))){
                $this->redirect('user/login');
            }
        }
        $presets=array(
        		'/babage/articles/edit',
        		'/babage/categories/index',
        		'/babage/articles/alist',
        		'/babage/articles/add',
        		'/babage/articles/recycle'
        	);
        $this->am_in=in_array(strtolower(__ACTION__),$presets)?1:0;
    }

    public function _empty($method, $args){
        $this->redirect('/babage');
    }
}