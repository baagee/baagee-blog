<?php
return array(
    //显示页面跟踪信息
    'SHOW_PAGE_TRACE'=>true,
    'DEFAULT_CHARSET'       =>  'utf-8', // 默认输出编码
    'MODULE_ALLOW_LIST'     =>  array('Home','Babage'),   //可供访问的分组
    //默认分组设置
    'DEFAULT_MODULE'        =>  'Home',  // 默认模块
    'URL_MODULE_MAP' => array('babage'=>'admin'),
    'URL_CASE_INSENSITIVE'  =>  true,   //url不区分大小写
    //数据库配置
    'DB_TYPE'               =>  'mysqli',
    'DB_HOST'               =>  '127.0.0.1',
    'DB_NAME'               =>  'baageeblog',
    'DB_USER'               =>  'root',
    'DB_PWD'                =>  'root',
    'DB_PREFIX'             =>  'bb_',
    'HTML_FILE_SUFFIX'=>'.html',
    'URL_MODEL'=>'2',
    /*错误页面*/
    'ERROR_PAGE' =>'/Public/static/404/error.html',
    // 配置邮件发送服务器
    'MAIL_SMTP'=>array(
        'MAIL_HOST' =>F('webSet')['MAIL_HOST'],//smtp服务器的名称
        'MAIL_SMTPAUTH' =>TRUE, //启用smtp认证
        'MAIL_USERNAME' =>F('webSet')['MAIL_USERNAME'],//你的邮箱名
        'MAIL_FROM' =>F('webSet')['MAIL_FROM'],//发件人地址
        'MAIL_FROMNAME'=>F('webSet')['MAIL_FROMNAME'],//发件人姓名
        'MAIL_PASSWORD' =>F('webSet')['MAIL_PASSWORD'],//邮箱密码
        'MAIL_CHARSET' =>'utf-8',//设置邮件编码
        'MAIL_ISHTML' =>TRUE, // 是否HTML格式邮件
    ),
    'LOG_RECORD' => true, // 开启日志记录
    'LOG_LEVEL'  =>'EMERG,ALERT,CRIT,ERR', // 只记录EMERG ALERT CRIT ERR 错误
);