<?php
return array(
    'HTML_CACHE_ON'     =>    true, // 开启静态缓存
    'HTML_CACHE_TIME'   =>    3600*24,   // 全局静态缓存有效期（秒）
    'HTML_FILE_SUFFIX'  =>    '.html', // 设置静态缓存文件后缀
    'HTML_CACHE_RULES'  =>     array(  // 定义静态缓存规则
        // 定义格式1 数组方式,文章页缓存一天
        // 'index:article'    =>     array('article_{id}', 24*3600),
        //关于我
        'index:about'		=>		array('about',24*3600),
        /*友情链接*/
        'index:links'      =>      array('links',24*3600),
        /*本站标签*/
        'index:tags'		=>		array('tags',24*3600),
    ),
);