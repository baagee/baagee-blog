/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306本机
Source Server Version : 50538
Source Host           : 127.0.0.1:3306
Source Database       : baageeblog

Target Server Type    : MYSQL
Target Server Version : 50538
File Encoding         : 65001

Date: 2017-08-09 21:52:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bb_articles
-- ----------------------------
DROP TABLE IF EXISTS `bb_articles`;
CREATE TABLE `bb_articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `views` int(11) NOT NULL,
  `likes` int(11) NOT NULL DEFAULT '0',
  `tags` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `cover` varchar(100) DEFAULT NULL,
  `banner` varchar(100) DEFAULT NULL,
  `is_banner` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bb_article_contents
-- ----------------------------
DROP TABLE IF EXISTS `bb_article_contents`;
CREATE TABLE `bb_article_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=153 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bb_categories
-- ----------------------------
DROP TABLE IF EXISTS `bb_categories`;
CREATE TABLE `bb_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bb_comments
-- ----------------------------
DROP TABLE IF EXISTS `bb_comments`;
CREATE TABLE `bb_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `comment` text NOT NULL COMMENT '评论内容文本',
  `to` int(11) NOT NULL DEFAULT '0' COMMENT '给谁回复的id',
  `browser` varchar(55) DEFAULT NULL COMMENT '用户浏览器',
  `systemos` varchar(55) DEFAULT NULL COMMENT '用户设备系统',
  `email` varchar(100) DEFAULT NULL COMMENT '用户的email',
  `article_id` int(11) NOT NULL COMMENT '文章id',
  `nickname` varchar(55) DEFAULT NULL COMMENT '用户昵称',
  `avatar` varchar(120) DEFAULT NULL COMMENT '头像',
  `vid` int(11) DEFAULT NULL COMMENT '用户id',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为admin后台回复',
  `upc` int(11) NOT NULL DEFAULT '0' COMMENT '点赞',
  `downc` int(11) NOT NULL DEFAULT '0' COMMENT '踩',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bb_links
-- ----------------------------
DROP TABLE IF EXISTS `bb_links`;
CREATE TABLE `bb_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL,
  `describe` text,
  `create_time` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bb_menus
-- ----------------------------
DROP TABLE IF EXISTS `bb_menus`;
CREATE TABLE `bb_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) NOT NULL,
  `url` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `icon_class` varchar(55) NOT NULL,
  `order` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bb_messages
-- ----------------------------
DROP TABLE IF EXISTS `bb_messages`;
CREATE TABLE `bb_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(55) DEFAULT NULL,
  `message` text NOT NULL,
  `create_time` int(11) NOT NULL,
  `to` int(11) NOT NULL DEFAULT '0',
  `email` varchar(55) DEFAULT NULL,
  `browser` varchar(55) DEFAULT NULL,
  `systemOS` varchar(55) DEFAULT NULL,
  `avatar` varchar(120) DEFAULT NULL,
  `vid` int(11) DEFAULT NULL,
  `downm` int(11) NOT NULL DEFAULT '0',
  `upm` int(11) NOT NULL DEFAULT '0',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=133 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bb_user
-- ----------------------------
DROP TABLE IF EXISTS `bb_user`;
CREATE TABLE `bb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(55) NOT NULL,
  `password` varchar(255) NOT NULL,
  `description` text,
  `update_time` int(11) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `qq` varchar(12) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `weibo` varchar(55) DEFAULT NULL,
  `weibo_num` varchar(11) DEFAULT NULL,
  `gitosc` varchar(55) DEFAULT NULL,
  `github` varchar(55) DEFAULT NULL,
  `login_ip` varchar(10) DEFAULT NULL,
  `last_login_time` int(11) DEFAULT NULL,
  `login_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for bb_visitors
-- ----------------------------
DROP TABLE IF EXISTS `bb_visitors`;
CREATE TABLE `bb_visitors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(40) NOT NULL,
  `gender` varchar(4) NOT NULL,
  `province` varchar(32) NOT NULL,
  `city` varchar(32) NOT NULL,
  `birthyear` varchar(4) NOT NULL,
  `figureurl` varchar(128) NOT NULL,
  `nickname` varchar(64) NOT NULL,
  `login_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- insert menus default data
-- ----------------------------
INSERT INTO `bb_menus` VALUES ('8', '网站首页', '/', '1', 'glyphicon glyphicon-home', '7');
INSERT INTO `bb_menus` VALUES ('9', '留言墙', '/index/messagewall.html', '1', 'glyphicon glyphicon-erase', '4');
INSERT INTO `bb_menus` VALUES ('10', '友情链接', '/index/links.html', '1', 'glyphicon glyphicon-link', '3');
INSERT INTO `bb_menus` VALUES ('11', '本站标签', '/index/tags.html', '1', 'glyphicon glyphicon-tags', '2');
INSERT INTO `bb_menus` VALUES ('12', '关于我', '/index/about.html', '1', 'glyphicon glyphicon-user', '1');
