<?php
namespace Vendor\qq;

class qqAuth{

	//申请到的appid
	private $appid='';
	//申请到的appkey
	private $appkey='';
	//QQ登录成功后跳转的地址,请确保地址真实可用，否则会导致登录失败。
	private $callback='';
	//QQ授权api接口.按需调用
	private $scope='';

	public function __construct($appid,$appkey,$callback,$scope=''){
		$this->appid=$appid;
		$this->appkey=$appkey;
		$this->callback=$callback;
		if(empty($scope)){
			$this->scope='get_user_info,add_share,list_album,add_album,
				upload_pic,add_topic,add_one_blog,add_weibo';			
		}else{
			$this->scope=$scope;
		}
	}

	/**
	 * QQ登录
	 */
	function qqLogin(){
	    $_SESSION['state'] = md5(uniqid(rand(), TRUE)); //CSRF 保护
	    $login_url = "https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id=" 
	        . $this->appid . "&redirect_uri=" . urlencode($this->callback)
	        . "&state=" . $_SESSION['state']
	        . "&scope=".$this->scope;
	    header("Location:$login_url");
	}

	/**
	 * 回调函数
	 */
	function qqCallback(){
	    if($_REQUEST['state'] == $_SESSION['state']){
	        $token_url = "https://graph.qq.com/oauth2.0/token?grant_type=authorization_code&"
	            . "client_id=" . $this->appid. "&redirect_uri=" . urlencode($this->callback)
	            . "&client_secret=" . $this->appkey. "&code=" . $_REQUEST["code"];
	        $response = file_get_contents($token_url);
	        if (strpos($response, "callback") !== false){
	            $lpos = strpos($response, "(");
	            $rpos = strrpos($response, ")");
	            $response  = substr($response, $lpos + 1, $rpos - $lpos -1);
	            $msg = json_decode($response);
	            if (isset($msg->error)){
	                echo "<h3>error:</h3>" . $msg->error;
	                echo "<h3>msg  :</h3>" . $msg->error_description;
	                exit;
	            }
	        }
	        $params = array();
	        parse_str($response, $params);
	        $_SESSION["access_token"] = $params["access_token"];
	    }else{
	        echo("The state does not match. You may be a victim of CSRF.");
	    }
	}

	/**
	 * 获取用户openid
	 */
	function getOpenid(){
	    $graph_url = "https://graph.qq.com/oauth2.0/me?access_token=" 
	        . $_SESSION['access_token'];
	    $str  = file_get_contents($graph_url);
	    if (strpos($str, "callback") !== false){
	        $lpos = strpos($str, "(");
	        $rpos = strrpos($str, ")");
	        $str  = substr($str, $lpos + 1, $rpos - $lpos -1);
	    }
	    $user = json_decode($str);
	    if (isset($user->error)){
	        echo "<h3>error:</h3>" . $user->error;
	        echo "<h3>msg  :</h3>" . $user->error_description;
	        exit;
	    }
	    $_SESSION["openid"] = $user->openid;
	}

	/**
	 * 获取用户信息
	 */
	function getUserInfo(){
	    $get_user_info = "https://graph.qq.com/user/get_user_info?"
	        . "access_token=" . $_SESSION['access_token']
	        . "&oauth_consumer_key=" . $this->appid
	        . "&openid=" . $_SESSION["openid"]
	        . "&format=json";
	    $info = file_get_contents($get_user_info);
	    $arr = json_decode($info, true);
	    return $arr;
	}

}