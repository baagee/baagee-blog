(function($) {
  'use strict';

  $(function() {
    var $fullText = $('.admin-fullText');
    $('#admin-fullscreen').on('click', function() {
      $.AMUI.fullscreen.toggle();
    });

    $(document).on($.AMUI.fullscreen.raw.fullscreenchange, function() {
      $fullText.text($.AMUI.fullscreen.isFullscreen ? '退出全屏' : '开启全屏');
    });
  });
})(jQuery);

  /*全选，取消全选*/
var checkall=document.getElementsByName("selectme");  
//全选
function select(){
  for(var $i=0;$i<checkall.length;$i++){  
    checkall[$i].checked=true;  
  }  
};
//反选
function reverse(){
  for(var $i=0;$i<checkall.length;$i++){  
    if(checkall[$i].checked){  
      checkall[$i].checked=false;  
    }else{  
      checkall[$i].checked=true;  
    }  
  }  
}     
//全不选     
function noselect(){ 
  for(var $i=0;$i<checkall.length;$i++){  
    checkall[$i].checked=false;  
  }  
} 
